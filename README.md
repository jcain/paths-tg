# Paths TG

Paths TG is a library for working with paths in PHP 7.2+.


## Installation

```batchfile
composer require jcain/paths-tg
```


## Component stability statuses

| Component                                                                                     | Stability | Since |
|:----------------------------------------------------------------------------------------------|:---------:|:-----:|
| [Path](src/Path.api.md)                                                                       | alpha     | 0.0   |
| [PathParser](src/PathParser.api.md)                                                           | alpha     | 0.0   |
| [PathResolver](src/PathResolver.api.md)                                                       | alpha     | 0.0   |
| [PathResolverStringifier](src/PathResolverStringifier.api.md)                                 | alpha     | 0.0   |
| [PathStringifier](src/PathStringifier.api.md)                                                 | alpha     | 0.0   |
| [PathSystem](src/PathSystem.api.md)                                                           | alpha     | 0.0   |
| [Basic/BasicPath](src/Basic/BasicPath.api.md)                                                 | alpha     | 0.0   |
| [Basic/BasicPathParser](src/Basic/BasicPathParser.api.md)                                     | alpha     | 0.0   |
| [Basic/BasicPathResolver](src/Basic/BasicPathResolver.api.md)                                 | alpha     | 0.0   |
| [Basic/BasicPathResolverStringifierTrait](src/Basic/BasicPathResolverStringifierTrait.api.md) | alpha     | 0.0   |
| [Basic/BasicPathResolverTrait](src/Basic/BasicPathResolverTrait.api.md)                       | alpha     | 0.0   |
| [Basic/BasicPathStringifier](src/Basic/BasicPathStringifier.api.md)                           | alpha     | 0.0   |
| [Basic/BasicPathSystem](src/Basic/BasicPathSystem.api.md)                                     | alpha     | 0.0   |

The **Stability** column indicates the component's stability status, and the **Since** column indicates the package version when the component first achieved that stability.

Each component and its members has a **stability status** indicating how stable the interface and implementation is to depend on in production. The stability may be one of the following:

* **alpha**: The interface and implementation are unstable and may change significantly.
* **beta**: The interface is stable but its implementation is not sufficiently tested.
* **omega**: The interface and implementation are stable and considered ready for production use.

A component's stability status is the same as the highest stability status of its members. Once a member's stability is raised, it will not be reduced.


## License

Paths TG is licensed under the MIT license. See the [LICENSE](LICENSE.md) file for more information.