<?php namespace JCain\Paths\TG\Basic;

use \JCain\Paths\TG\Path;


class BasicPath implements Path {
	private const FLAG_ABSOLUTE  = 0x01;
	private const FLAG_DIRECTORY = 0x02;

	private $segments;
	private $flags;


	public function __construct(array $segments, bool $isAbsolute = false, bool $isDirectory = false) {
		// TODO: Assert segments are strings. (jc)
		$this->segments = $segments;
		$this->flags = ($isAbsolute ? self::FLAG_ABSOLUTE : 0) | ($isDirectory ? self::FLAG_DIRECTORY : 0);
	}


	//
	// Methods
	//


	protected function createPath(array $segments, bool $isAbsolute, bool $isDirectory) {
		return new self($segments, $isAbsolute, $isDirectory);
	}


	//
	// Path Implementation
	//


	public function segments() : array {
		return $this->segments;
	}


	public function isAbsolute() : bool {
		return !!($this->flags & self::FLAG_ABSOLUTE);
	}


	public function isDirectory() : bool {
		return !!($this->flags & self::FLAG_DIRECTORY);
	}


	public function append(string ...$segments) : Path {
		return $this->createPath(array_merge($this->segments, $segments), $this->isAbsolute(), $this->isDirectory());
	}


 	public function shorten(int $segments) : Path {
		if (!$segments || $segments >= count($this->segments))
			return $this;

		return $this->createPath(array_slice($this->segments, 0, $segments), $this->isAbsolute(), true);
	}


	public function dropFile() : Path {
		if (!$this->segments || $this->isDirectory())
			return $this;

		return $this->createPath(array_slice($this->segments, 0, count($this->segments) - 1, $this->isAbsolute, true));
	}


	public function asAbsolute() : Path {
		return ($this->isAbsolute() ? $this : $this->createPath($this->segments, true, $this->isDirectory()));
	}


	public function asRelative() : Path {
		return (!$this->isAbsolute() ? $this : $this->createPath($this->segments, false, $this->isDirectory()));
	}


	public function asDirectory() : Path {
		return ($this->isDirectory() ? $this : $this->createPath($this->segments, $this->isAbsolute(), true));
	}


	public function asFile() : Path {
		return (!$this->isDirectory() ? $this : $this->createPath($this->segments, $this->isAbsolute(), false));
	}


	public function count() {
		return count($this->segments);
	}


	public function __toString() {
		$segments = $this->segments();
		$str = ($this->isAbsolute() ? '/' : '');
		// TODO: Escape the / and % characters into URL escape sequences. (jc)
		$str .= implode('/', $segments);
		$str .= ($segments && $this->isDirectory() ? '/' : '');
		return $str;
	}


	//
	// Static Members
	//


	static public function empty() {
		static $instance;
		if (!$instance)
			$instance = new self([]);
		return $instance;
	}
}