<?php namespace JCain\Paths\TG\Basic;

use \JCain\Paths\TG\Path;
use \JCain\Paths\TG\PathParser;


class BasicPathParser implements PathParser {
	private $separators;
	private $alternate;


	public function __construct(array $config = []) {
		$this->separators = $config['separators'] ?? '/\\';
		$this->allowDriveLetter = ($config['allowDriveLetter'] !== null ? !!$config['allowDriveLetter'] : true);
	}


	//
	// Methods
	//


	public function createPath(array $segments, bool $absolute, bool $directory) {
		return new BasicPath($segments, $absolute, $directory);
	}


	//
	// PathParser Implementation
	//


	public function parse(string $path) : Path {
		if ($path === '')
			return $this->createPath([], false, false);

		$separator = $this->separators[0];

		// Replace the alternate separators with the normalized one.
		$separatorsLen = strlen($this->separators);
		if ($separatorsLen > 1) {
			for ($i = 1; $i < $separatorsLen; $i++) {
				$alternate = $this->separators[$i];
				$path = str_replace($alternate, $separator, $path);
			}
		}

		$len = strlen($path);
		$isDirectory = $path[$len - 1] === $separator;
		$isAbsolute = $path[0] === $separator;

		if (!$isAbsolute && $this->allowDriveLetter && preg_match('/^[A-Za-z]:\//', $path))
			$isAbsolute = true;

		$segments = explode($separator, $path);
		if ($isDirectory)
			array_pop($segments);
		if ($isAbsolute)
			array_shift($segments);

		return $this->createPath($segments, $isAbsolute, $isDirectory);
	}
}