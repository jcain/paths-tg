<?php namespace JCain\Paths\TG\Basic;

use \JCain\Paths\TG\Path;
use \JCain\Paths\TG\PathResolver;


class BasicPathResolver implements PathResolver {
	use BasicPathResolverTrait;

	private $keepEmptySegments;


	public function __construct(array $config = []) {
		$this->keepEmptySegments = !!$config['keepEmptySegments'];
	}


	//
	// Methods
	//


	public function createPath(array $segments, bool $absolute, bool $directory) {
		return new BasicPath($segments, $absolute, $directory);
	}


	protected function parsePath($path, $index) : Path {
		if (!($path instanceof Path))
			throw new \InvalidArgumentException();

		return $path;
	}


	//
	// PathResolver Implementation
	//


	public function resolve(...$paths) : Path {
		$destSegments = [];
		$destIndex = 0;
		$destIsAbsolute = false;
		$destIsDirectory = false;

		foreach ($paths as $index => $path) {
			$srcPath = $this->parsePath($path, $index);

			if ($srcPath->isAbsolute()) {
				$destIndex = 0;
				$destIsAbsolute = true;
			}
			else if ($destIndex && !$destIsDirectory) {
				$destIndex--;
			}
			$destIsDirectory = $srcPath->isDirectory();

			for ($srcSegments = $srcPath->segments(), $srcIndex = 0, $srcCount = count($srcSegments); $srcIndex < $srcCount; $srcIndex++) {
				$srcSegment = $srcSegments[$srcIndex];

				if ($srcSegment === '') {
					if ($this->keepEmptySegments)
						$destSegments[$destIndex++] = $srcSegment;
				}
				else if ($srcSegment === Path::CURRENT_DIRECTORY) {
					// Do nothing.
				}
				else if ($srcSegment === Path::PARENT_DIRECTORY) {
					if ($destIndex) {
						if ($destIndex === 0 || $destSegments[$destIndex - 1] === Path::PARENT_DIRECTORY)
							$destSegments[$destIndex++] = Path::PARENT_DIRECTORY;
						else
							$destIndex--;
					}
				}
				else {
					$destSegments[$destIndex++] = $srcSegment;
				}
			}
		}

		// Remove the rest.
		array_splice($destSegments, $destIndex);

		return $this->createPath($destSegments, $destIsAbsolute, $destIsDirectory);
	}
}