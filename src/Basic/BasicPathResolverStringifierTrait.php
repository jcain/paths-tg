<?php namespace JCain\Paths\TG\Basic;


trait BasicPathResolverStringifierTrait {
	use BasicPathResolverTrait;


	public function resolveAndStringify(...$paths) : string {
		$path = $this->resolve(...$paths);
		$str = $this->stringify($path);
		return $str;
	}
}