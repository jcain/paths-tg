<?php namespace JCain\Paths\TG\Basic;

use \JCain\Paths\TG\Path;


trait BasicPathResolverTrait {
	public abstract function resolve(...$paths) : Path;


	public function equals(Path $pathA, Path $pathB) : bool {
		if ($pathA->isDirectory() !== $pathB->isDirectory())
			return false;

		if ($pathA->isAbsolute() !== $pathB->isAbsolute())
			return false;

		$pathA = $this->resolve($pathA);
		$pathB = $this->resolve($pathB);

		$segmentsA = $pathA->segments();
		$segmentsB = $pathB->segments();
		if (count($segmentsA) !== count($segmentsB))
			return false;

		for ($i = 0, $len = count($segmentsA); $i < $len; $i++) {
			if ($segmentsA[$i] !== $segmentsB[$i])
				return false;
		}

		return true;
	}


	public function contains(Path $pathA, Path $pathB) : bool {
		if (!$pathA->isDirectory())
			return false;

		if ($pathA->isAbsolute() !== $pathB->isAbsolute())
			return false;

		$pathA = $this->resolve($pathA);
		$pathB = $this->resolve($pathB);

		$segmentsA = $pathA->segments();
		$segmentsB = $pathB->segments();
		if (count($segmentsA) >= count($segmentsB))
			return false;

		for ($i = 0, $len = count($segmentsA); $i < $len; $i++) {
			if ($segmentsA[$i] !== $segmentsB[$i])
				return false;
		}

		return true;
	}
}