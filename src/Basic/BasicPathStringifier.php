<?php namespace JCain\Paths\TG\Basic;

use \JCain\Paths\TG\Path;
use \JCain\Paths\TG\PathStringifier;


class BasicPathStringifier implements PathStringifier {
	private $separator;


	public function __construct(array $config = []) {
		// TODO: Assert values. (jc)
		$this->separator = $config['separator'] ?? DIRECTORY_SEPARATOR;
	}


	//
	// Methods
	//


	protected function escapeSegment(string $segment, int $index) {
		// TODO: Check that the segment does not contain invalid characters, or escape them if possible. (jc)
		return $segment;
	}


	//
	// PathStringifier Implementation
	//


	public function stringify(Path $path) : string {
		$segments = $path->segments();

		// TODO: Conditionally prefix with '.'. (jc)
		$str = ($path->isAbsolute() ? $this->separator : '');

		$len = count($segments);
		if ($len) {
			$str .= $this->escapeSegment($segments[0], 0);
			for ($i = 1; $i < $len; $i++)
				$str .= $this->separator . $this->escapeSegment($segments[$i], $i);
		}

		$str .= ($len && $path->isDirectory() ? $this->separator : '');

		return $str;
	}
}