<?php namespace JCain\Paths\TG\Basic;

use \JCain\Paths\TG\Path;
use \JCain\Paths\TG\PathParser;
use \JCain\Paths\TG\PathResolver;
use \JCain\Paths\TG\PathStringifier;
use \JCain\Paths\TG\PathSystem;


class BasicPathSystem implements PathSystem {
	use BasicPathResolverStringifierTrait;

	private $parser;
	private $resolver;
	private $stringifier;


	public function __construct(array $config = []) {
		// TODO: Add asserts. (jc)
		// TODO: Allow configuring the default basic instances? (jc)
		$separator = $config['separator'];

		$this->parser = $config['parser'] ?? new BasicPathParser([ 'separator' => $separator ]);
		$this->resolver = $config['resolver'] ?? new BasicPathResolver();
		$this->stringifier = $config['stringifier'] ?? new BasicPathStringifier([ 'separator' => $separator ]);
	}


	//
	// Methods
	//


	public function parser() : PathParser {
		return $this->parser;
	}


	public function resolver() : PathResolver {
		return $this->resolver;
	}


	public function stringifier() : PathStringifier {
		return $this->stringifier;
	}


	//
	// PathSystem Implementation
	//


	public function parse(string $path) : Path {
		return $this->parser->parse($path);
	}


	public function resolve(...$paths) : Path {
		$objs = [];
		foreach ($paths as $path) {
			if (gettype($path) === 'string')
				$objs[] = $this->parse($path);
			else if ($path instanceof Path)
				$objs[] = $path;
			else
				throw new \InvalidArgumentException();
		}

		return $this->resolver->resolve(...$objs);
	}


	public function stringify(Path $path) : string {
		return $this->stringifier->stringify($path);
	}
}