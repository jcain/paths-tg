# JCain\Paths\TG\Path


## Path interface

_Stability: **alpha**, Since: **0.0**_

```php
interface Path {
  // Consts
  const CURRENT_DIRECTORY = '.';
  const PARENT_DIRECTORY = '..';

  // Methods
  function segments() : array
  function isAbsolute() : bool
  function isDirectory() : bool
  function append(...$segments : string) : Path
  function shorten($segments : int) : Path
  function dropFile() : Path
  function asAbsolute() : Path
  function asRelative() : Path
  function asDirectory() : Path
  function asFile() : Path
}
```


### Methods


#### segments()

```php
function segments() : array
```


#### isAbsolute()

```php
function isAbsolute() : bool
```


#### isDirectory()

```php
function isDirectory() : bool
```


#### append()

```php
function append(...$segments : string) : Path
```


#### shorten()

```php
function shorten($segments : int) : Path
```


#### dropFile()

```php
function dropFile() : Path
```


#### asAbsolute()

```php
function asAbsolute() : Path
```


#### asRelative()

```php
function asRelative() : Path
```


#### asDirectory()

```php
function asDirectory() : Path
```


#### asFile()

```php
function asFile() : Path
```