<?php namespace JCain\Paths\TG;


/// Segments are 0 or more unescaped strings
/// The number of '..' cannot exit the root
/// A path is resolved if it doesn't contain '.' or '..'
/// A path is bare if it is relative and doesn't start with '.' or '..'
/// Implementations must be immutable: the methods here must return the same value for the lifetime of the instance.
interface Path extends \Countable {
	const CURRENT_DIRECTORY = '.';
	const PARENT_DIRECTORY = '..';


	function segments() : array;


	function isAbsolute() : bool;


	function isDirectory() : bool;


	function append(string ...$segments) : Path;


	function shorten(int $segments) : Path;


	function dropFile() : Path;


	function asAbsolute() : Path;


	function asRelative() : Path;


	function asDirectory() : Path;


	function asFile() : Path;


	function __toString();
}