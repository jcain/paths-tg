# JCain\Paths\TG\PathParser


## PathParser interface

_Stability: **alpha**, Since: **0.0**_

```php
interface PathParser {
  // Methods
  function parse($path : string) : Path
}
```


### Methods


#### parse()

```php
function parse($path : string) : Path
```