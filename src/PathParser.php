<?php namespace JCain\Paths\TG;


/// Defines the separator characters.
/// May check syntax of segments.
/// May impose limits on the path semantics (e.g. no '.' or '..', must be absolute).
/// Uses a particular implementation of Path.
interface PathParser {
	function parse(string $path) : Path;
}