# JCain\Paths\TG\PathResolver


## PathResolver interface

_Stability: **alpha**, Since: **0.0**_

```php
interface PathResolver {
  // Methods
  function resolve(...$paths : any) : Path
  function equals($pathA : Path, $pathB : Path) : bool
  function contains($pathA : Path, $pathB : Path) : bool
}
```

Resolves '.' and '..'.

May throw exceptions in special cases (e.g. relative path exits parent).


### Methods


#### resolve()

```php
function resolve(...$paths : any) : Path
```

Must take Path instances, but implementations may allow other types that can be reliably and predictably parsed/converted to Path.

The result Path is normalized, removing empty segments and resolving '.' and '..'.

With no arguments, a Path of (segments = [], isAbsolute = false, and isDirectory = false) is returned.

With one argument, the returned Path is normalized, and may be the same instance if no changes were made.


#### equals()

```php
function equals($pathA : Path, $pathB : Path) : bool
```

Returns true if `$pathA->isAbsolute()` is equal to `$pathB->isAbsolute()`, `$pathA->isDirectory()` is equal to `$pathB->isDirectory()`, and if all (normalized) segments are equal.

Segment equality may depend on case sensitivity.


#### contains()

```php
function contains($pathA : Path, $pathB : Path) : bool
```

Returns true if `$pathA->isAbsolute()` is equal to `$pathB->isAbsolute()`, `$pathA->isDirectory()` is true, `$pathA` is shorter than `$pathB`, and `$pathB`'s segments are the first of `$pathA`'s segments.

Segment equality may depend on case sensitivity.