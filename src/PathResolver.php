<?php namespace JCain\Paths\TG;


interface PathResolver {
	function resolve(...$paths) : Path;


	function equals(Path $pathA, Path $pathB) : bool;


	function contains(Path $pathA, Path $pathB) : bool;
}