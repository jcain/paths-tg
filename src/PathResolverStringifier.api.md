# JCain\Paths\TG\PathResolverStringifier


## PathResolverStringifier interface

_Stability: **alpha**, Since: **0.0**_

```php
interface PathResolverStringifier extends PathResolver, PathStringifier {
  // Methods
  function resolveAndStringify(...$paths : any) : string
}
```


### Methods


#### resolveAndStringify()

```php
function resolveAndStringify(...$paths : any) : string
```

Must take Path instances, but implementations may allow other types that can be reliably parsed/converted to Path.