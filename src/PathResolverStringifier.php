<?php namespace JCain\Paths\TG;


interface PathResolverStringifier extends PathResolver, PathStringifier {
	function resolveAndStringify(...$paths) : string;
}