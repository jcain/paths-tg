# JCain\Paths\TG\PathStringifier


## PathStringifier interface

_Stability: **alpha**, Since: **0.0**_

```php
interface PathStringifier {
  // Methods
  function stringify($path : Path) : string
}
```

Defines the separator characters.

May check syntax of segments.

May impose limits on the path semantics (e.g. no '.' or '..', must be absolute).


### Methods


#### stringify()

```php
function stringify($path : Path) : string
```