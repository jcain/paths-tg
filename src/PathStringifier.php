<?php namespace JCain\Paths\TG;


interface PathStringifier {
	function stringify(Path $path) : string;
}