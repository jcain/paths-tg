# JCain\Paths\TG\PathSystem


## PathSystem interface

_Stability: **alpha**, Since: **0.0**_

```php
interface PathSystem extends PathParser, PathResolverStringifier {
}
```